package main

import (
	"errors"
	"fmt"
	"gopkg.in/yaml.v3"
	"log"
	"net/http"
	"os"
)

type Info struct {
	Version     string `yaml:"version"`
	Title       string `yaml:"title"`
	Description string `yaml:"description"`
}

type Paths struct {
	Path map[string]Path
}

type Path struct {
	Method map[string]Method
}

type Method struct {
	Summary     string      `yaml:"summary"`
	Description string      `yaml:"description"`
	Parameters  []Parameter `yaml:"parameters"`
}

type Parameter struct {
	In          string `yaml:"in"`
	Name        string `yaml:"name"`
	Description string `yaml:"description"`
	Required    bool   `yaml:"required"`
	Schema      Schema `yaml:"schema"`
}

type Schema struct {
	Type string `yaml:"type"`
}

type Definition struct {
	Info     Info     `yaml:"info"`
	BasePath string   `yaml:"basePath"`
	Swagger  string   `yaml:"swagger"`
	Consumes []string `yaml:"consumes"`
	Produces []string `yaml:"produces"`
	Paths    Paths    `yaml:"paths"`
}

type Handler struct {
	Method string
	Path   string
	Url    string
}

func (paths *Paths) UnmarshalYAML(unmarshal func(interface{}) error) error {
	var path map[string]Path
	if err := unmarshal(&path); err != nil {
		var typeError *yaml.TypeError
		if !errors.As(err, &typeError) {
			return err
		}
	}
	paths.Path = path
	return nil
}

func (path *Path) UnmarshalYAML(unmarshal func(interface{}) error) error {
	var method map[string]Method
	if err := unmarshal(&method); err != nil {
		var typeError *yaml.TypeError
		if !errors.As(err, &typeError) {
			return err
		}
	}
	path.Method = method
	return nil
}

func main() {
	definitionsPath := os.Getenv("DEFINITIONS_PATH")
	if definitionsPath == "" {
		definitionsPath = "definitions"
	}
	dir, err := readDir(definitionsPath)
	if err != nil {
		log.Fatalf("Could not read definitions from `%s` with `%s`\n", definitionsPath, err)
	}
	log.Printf("Read files in directory %s\n", definitionsPath)
	definitions, err := parseYaml(dir)
	if err != nil {
		log.Fatalf("Could not parse yaml definitions with `%s`\n", err)
	}
	log.Printf("Parsed yaml files\n")
	processedDefinitions, err := processDefinitions(definitions)
	if err != nil {
		log.Fatalf("Could not process definitions with `%s`\n", err)
	}
	err = serveDefinitions(processedDefinitions)
	if err != nil {
		log.Fatalf("Server failed with `%s`\n", err)
	}
	log.Printf("Server stopped successfully\n")
}

func parseYaml(dir [][]byte) ([]Definition, error) {
	var definitions []Definition
	for _, file := range dir {
		var definition Definition
		err := yaml.Unmarshal(file, &definition)
		if err != nil {
			return nil, errors.New("could not parse the yaml")
		}
		definitions = append(definitions, definition)
	}
	return definitions, nil
}

func processDefinitions(definitions []Definition) ([]Handler, error) {
	//for _, definition := range definitions {
	//	for path, pathDescription := range definition.Paths.Path {
	//		for method, methodDescription := range pathDescription.Method {
	//			fmt.Println(path)
	//			fmt.Println(method)
	//			fmt.Println(methodDescription)
	//		}
	//	}
	//}
	// TODO: finish this method
	return nil, nil
}

func serveDefinitions(handlers []Handler) error {
	for _, handler := range handlers {
		http.HandleFunc(handler.Path, func(responseWriter http.ResponseWriter, request *http.Request) {
			switch handler.Method {
			case "get":
				{
					//response, err := http.Get(handler.Url)
					//if err != nil {
					//	return
					//}
				}
			case "post":
				{
				}
			case "patch":
				{

				}
			default:
				{
					log.Printf("Unknown HTTP method %s\n", handler.Method)
					responseWriter.WriteHeader(404)
					return
				}
			}
		})
	}
	port := os.Getenv("APP_PORT")
	if port == "" {
		port = "3000"
	}
	log.Printf("Server running on localhost:%s\n", port)
	return http.ListenAndServe(fmt.Sprintf(":%s", port), nil)
}

func readDir(path string) ([][]byte, error) {
	dirEntry, err := os.ReadDir(path)
	if err != nil {
		return [][]byte{}, errors.New("could not read directory " + path)
	}
	var result [][]byte
	for _, entry := range dirEntry {
		subPath := path + "/" + entry.Name()
		if entry.IsDir() {
			dir, err := readDir(subPath)
			if err != nil {
				return nil, errors.New("could not read directory " + subPath)
			}
			result = append(result, dir...)
		} else {
			file, err := os.ReadFile(subPath)
			if err != nil {
				return nil, errors.New("could not read file " + path)
			}
			result = append(result, file)
		}
	}
	return result, nil
}
